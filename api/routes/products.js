var express = require('express');
var router = express.Router();
var fs = require('fs');
var mongoose = require('mongoose');
var Product = require('../models/Product');

mongoose.Promise = require('bluebird');

mongoose.connect('mongodb://localhost/garba', { useMongoClient: true, promiseLibrary: require('bluebird') })
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));



/* GET products listing. */
router.get('/', function(req, res, next) {
    Product.find(function (err, products) {
        if (err) return next(err);
        res.json(products);
    });
});

router.get('/:id', function(req, res, next) {
  Product.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

router.post('/', function(req, res, next) {
    //Check if request is JSON
    if (!req.is('*/json'))
        res.status(400).sendStatus({ error: 'Request is not json' })

    Product.create(req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });

});

/* UPDATE Product */
router.put('/:id', function(req, res, next) {
    //Check if request is JSON
    if (!req.is('*/json'))
        res.status(400).sendStatus({ error: 'Request is not json' })

    Product.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

/* DELETE Product */
router.delete('/:id', function(req, res, next) {
    Product.findByIdAndRemove(req.params.id, req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});


module.exports = router;
