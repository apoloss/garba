var mongoose = require('mongoose');
var Schema = mongoose.Schema,

ObjectId = Schema.ObjectId;

var ProductSchema = new mongoose.Schema({
  _id: { type: ObjectId, auto: true },
  name: String,
  price: Number,
  list_price: Number,
  brand: String,
  category_id: Number,
  creation_date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Product', ProductSchema);