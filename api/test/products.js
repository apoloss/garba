process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('Products', () => {

  describe('/GET products', () => {
      it('it should GET all the products', (done) => {
        chai.request(server)
            .get('/products')
            .end((err, res) => {
                should.equal(err, null);
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('array');
              done();
            });
      });
      it('it should CREATE a product', (done) => {
        chai.request(server)
            .post('/products')
            .send({"name": "televisor 32 LED", "price": 19999.99, "list_price": 29999.99, "brand": "SONY", "category_id": 12345 })
            .end((err, res) => {
                should.equal(err, null);
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('name');
                res.body.should.have.property('price');
                res.body.should.have.property('list_price');
                res.body.should.have.property('_id');
              done();
            });
      });
      it('it should UPDATE a product', (done) => {
        chai.request(server)
          .get('/products')
          .end(function(err, res){
            chai.request(server)
              .put('/products/'+res.body[0]._id)
              .send({"name": "televisor 32 UHD", "price": 100, "list_price": 50, "brand": "SONY", "category_id": 12345 })
              .end((err, res) => {
                chai.request(server)
                  .get('/products/'+res.body._id)
                  .end(function(error, response){
                    should.equal(err, null);
                    response.should.have.status(200);
                    response.should.be.json;
                    response.body.should.be.a('object');
                    response.body.list_price.should.equal(50);

                  done();
                  });
              });
          });
      });
  });
});