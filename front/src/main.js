import Vue from 'vue'
import App from './App'
import numeral from 'numeral'
import VueFilter from 'vue-filter';
import axios from 'axios'
import VueAxios from 'vue-axios'
import ProductList from '@/components/ProductList'

import VueRouter from 'vue-router'

Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = process.env.API_URL;

Vue.use(VueRouter);
Vue.use(VueFilter);

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.router = new VueRouter({
    hashbang: false,
    linkActiveClass: 'active',
    mode: 'history',
    base: __dirname,
    routes: [
        {
          path: '/',
          name: 'home',
          component: ProductList,
          meta: {auth: true}

        }
    ]
})

new Vue({
  el: '#app',
  router: Vue.router,
  render: h => h(App)
})



