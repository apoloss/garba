# Garbarino Test

El test esta dividido en dos carpetas

  - api/
  - front/

# Consideraciones

El maquetado del sitio se penso mobile-first. Se asumieron criterios y definiciones en los diseÃ±os mobile/tablet.

## Front

El front fue hecho con Vuejs + Webpack. Para correrlo en modo dev, debemos ejecutar los siguientes comandos:

```sh
cd front/
npm run dev
```

## API
El front fue hecho con ExpressJS. Para correrlo en modo dev, debemos ejecutar los siguientes comandos:

```sh
cd api/
npm run dev
```

La api quedara expuesta en 127.0.0.1:3000

## Test
Se hicieron solamente dos test sencillos sobre la **api** que chequean la respuestas de los end-points. Para correrlos se deben ejecutar los siguientes comandos:

```sh
cd api/
npm run test
```